<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Questions extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['question_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['question_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['question_delete']['limit'] = 150; // 50 requests per hour per user/key
    }

    public function question_get()
    {
        $this->load->database();

        $query = $this->db->query('SELECT questionID, qTitle, qText, qAnswerObject, qCategory, qOrder, status FROM questions');

        $questions = json_encode($query->result());

        $questionID = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($questionID === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($questions)
            {
                // Set the response and exit
                $this->response($questions, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $questionID = (int) $this->get('id');

        // Validate the id.
        if ($questionID <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retreival.
        // Usually a model is to be used for this.

        $this->load->database();        

        $query = $this->db->get_where('questions', array('questionID' => $questionID));
        $question = json_encode($query->result());

        if ($question)
        {
            $this->set_response($question, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }

    }

    // Create new entry - send array by get method
    public function question_post()
    {
        $qTitle = $_POST['qTitle'];
        $Text = $_POST['qText'];
        $qAnswerObject = $_POST['qAnswerObject'];
        $qCategory = $_POST['qCategory'];
        $qOrder = $_POST['qOrder'];

        $message = array(
            'qTitle' => $qTitle,
            'qText' => $Text,
            'qAnswerObject' => $qAnswerObject,
            'qCategory' => $qCategory,
            'qOrder' => $qOrder
        );
        $this->load->database();
        $this->db->insert('questions', $message);
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    //UPADTE entry by put
    public function questionupdate_post()
    {
        $data = array(
            'qTitle' => $_POST['qTitle'],
            'qText' => $_POST['qText'],
            'qAnswerObject' => $_POST['qAnswerObject'],
            'qCategory' => $_POST['qCategory'],
            'qOrder' => $_POST['qOrder'],
			'questionID' => $_POST['qID'],
			'status' => $_POST['status']			
        );
        $this->load->database();

        $this->db->where('questionID', $_POST['qID']);
        $this->db->update('questions', $data);
        $this->set_response($data, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    //Delete user like: question/12
    //Router has to be configured
    public function question_delete()
    {
        $id = (int) $this->get('id');
        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        $this->load->database();
        $this->db->where('questionID', $id);
        $this->db->delete('questions');
        $this->set_response($id, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }
	
    public function usedquestions_get()
    {	
		$id = $_GET['id'];
        $this->load->database();
        $query = $this->db->query('SELECT questionID FROM questions WHERE qCategory ='.$id.' ');
        $questions = $query->result();		
		$this->set_response($questions, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
	}
	
	
    public function fq_get()
    {
		$id = $_GET['id'];
        $this->load->database();
        $query = $this->db->query('SELECT categories FROM block WHERE id ='.$id.'');		
        $categories = $query->result();
		//print_r($categories);
						
		$query = $this->db->query('SELECT questionID, qTitle, qText, qAnswerObject, status FROM questions WHERE qCategory IN ('.$categories[0]->categories.') AND status = 1');
		$questions = $query->result();		
		
		$this->set_response($questions, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
	}	
	
    public function fqadmin_get()
    {	
		//$id = (int) $this->get('cid');
		$id = $_GET['id'];
        $this->load->database();
        $query = $this->db->query('SELECT questionID, qTitle, qText, qAnswerObject, status FROM questions WHERE qCategory ='.$id.'');
        $questions = $query->result();		
		$this->set_response($questions, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
	}		
	

}
