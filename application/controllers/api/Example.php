<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Example extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function users_get()
    {
        $this->load->database();
        $users = "Users:[";

        $query = $this->db->query('SELECT id, name, email, fact FROM users');
        foreach ($query->result() as $row)
        {
            $users .="['id' => ".$row->id.", 'name' => ".$row->name.", 'email' => ".$row->email.", 'fact' => ".$row->fact."]";
        }
        $users .= "];";

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }


        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retreival.
        // Usually a model is to be used for this.

        $this->load->database();
        $user = "user:[";

        $query = $this->db->get_where('users', array('id' => $id));
        foreach ($query->result() as $row)
        {
            $user .="['id' => ".$row->id.", 'name' => ".$row->name.", 'email' => ".$row->email.", 'fact' => ".$row->fact."]";
        }
        $user .= "];";

        if ($user)
        {
            $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }

    }

    // Create new entry - send array by POST method
    public function users_post()
    {
        $id = $this->get('id');
        if(!isset($id)) {

            $this->load->database();
            $message = array(
                'name' => "Tamas",
                'email' => "dogiweb@live.com",
                'fact' => 'Added a resource'
            );
            $this->db->insert('users', $message);
            $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        }
    }

    //Delete user like: users/12
    public function users_delete()
    {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        ///delete from db
        $this->load->database();
        $this->db->where('id', $id);
        $this->db->delete('users');

        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];
        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

}
