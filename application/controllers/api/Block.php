<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Block extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['question_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['question_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['question_delete']['limit'] = 150; // 50 requests per hour per user/key
    }

    public function block_get()
    {
        $this->load->database();

        $query = $this->db->query('SELECT id, title, categories, status FROM block');

        $categories = json_encode($query->result());

        $categoryID = $this->get('id');

        // If the id parameter doesn't exist return all the users
        if ($categoryID === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($categories)
            {
                // Set the response and exit
				
                $this->response($categories, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.
        $categoryID = (int) $this->get('id');
        // Validate the id.
        if ($categoryID <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retreival.
        // Usually a model is to be used for this.
        $this->load->database();
        
        $query = $this->db->get_where('block', array('id' => $categoryID));
        $categories = json_encode($query->result());

        if ($categories)
        {
            $this->set_response($categories, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }

    }
	
    //UPADTE entry by put
    public function ublock_post()
    {
		
        $message = array(
            'title' => $_GET['title'],
            'categories' => $_GET['categories'],
			'status' => $_GET['status']
        );
		$id = $_GET['id'];
        $this->load->database();
        $this->db->where('id', $id);
        $this->db->update('block', $message);
        $this->set_response($message, REST_Controller::HTTP_OK); // UPDATED (201)OK being the HTTP response code
    }	

    // Create new entry - send array by get method
    public function block_post()
    {		
        $message = array(
            'title' => $_POST['title'],
            'categories' => $_POST['categories']
        );
        $this->load->database();
        $this->db->insert('block', $message);
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }


    //Delete user like: question/12
    //Router has to be configured
    public function block_delete()
    {
		
        echo $id = (int) $this->get('id');
		
        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        $this->load->database();
        $this->db->where('id', $id);
        $this->db->delete('block');
        $this->set_response($id, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }
	
	
	

}
